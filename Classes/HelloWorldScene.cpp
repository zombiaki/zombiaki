#include "HelloWorldScene.h"
#include <ctime>



USING_NS_CC;

HelloWorld* warstwa;													//////  WSKA�NIK NA WARSTW�
Gracz * player,*gracz;															 //////// WSKA�NIK NA GRACZA

auto iloscLudzi=10;	
auto iloscDrzew =10;
TMXTiledMap * _mapa;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
   // auto scene = Scene::create();                                               /////////////////FIZYKA
      auto scene = Scene::createWithPhysics();                                   /////////////////FIZYKA
	  scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);   /////////////////FIZYKA
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();
	layer->SetPhysicsWorld(scene->getPhysicsWorld()); /////////////////FIZYKA
    // add layer as a child to scene
    scene->addChild(layer);
	scene->getPhysicsWorld()->setGravity(Vec2(0,0));

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{

	srand(time(NULL));
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
	Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
	
///tilemap
	mapa = TMXTiledMap::create("desert.tmx");
	tlo=mapa->layerNamed("T�o");   //bez tego?
	addChild(mapa, -1);
	//koniec mapy


  
  kolejny = Sprite::create("Target.png");
  kolejny->setPosition(230+ kolejny->getContentSize().width, 200 );
  HelloWorld::addChild(kolejny);

  warstwa=this;   
  _mapa= mapa;

  gracz = new Gracz;
  player= new Gracz;
	 player = gracz;
  for(int i=0;i<iloscLudzi;i++)
  {
	gracz->nastepny= new Gracz;
	gracz=gracz->nastepny;
	gracz->nastepny=0;
  }
  gracz=player;

  Wieza blabla;
	

	Drzewo d1[10]; 

	
	auto krawedzBody = PhysicsBody::createEdgeBox(mapa->getContentSize(), PHYSICSBODY_MATERIAL_DEFAULT, 3 );
	auto krawedzNode = Node::create();
	krawedzNode->setPosition(mapa->getContentSize()/2);
	krawedzNode->setPhysicsBody(krawedzBody);
	this-> addChild(krawedzNode);
		  this->scheduleUpdate();



		  auto sluchaczKlawy = EventListenerKeyboard::create();  
	sluchaczKlawy->onKeyPressed=CC_CALLBACK_2(HelloWorld::poWcisnieciuKlawisza, this );
	sluchaczKlawy->onKeyReleased=CC_CALLBACK_2(HelloWorld::poZwolnieniuKlawisza, this );
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(sluchaczKlawy, this);
	
    return true;
}

void HelloWorld::update(float dt)
{
	warstwa->setPosition(-gracz->gracz->getPosition()+Director::getInstance()->getVisibleSize()/2);
	
	auto przesun = MoveBy::create(dt,Vec2(gracz->Vx, gracz->Vy));
	if (gracz!=0) gracz->gracz->runAction(przesun);

	 
}



bool HelloWorld::onTacz( cocos2d::Touch * touch, cocos2d::Event * event ,cocos2d::Sprite* kolejny)
{
	
	
		return true;
	
}


void HelloWorld::poWcisnieciuKlawisza( cocos2d::EventKeyboard::KeyCode klawisz, cocos2d::Event * event )
{ 
	if(klawisz==EventKeyboard::KeyCode::KEY_S)
	{
			gracz->Vy=-1;
	}
	else
	{
		if(klawisz==EventKeyboard::KeyCode::KEY_W)
	{
			gracz->Vy=1;
	}
	else{
		if(klawisz==EventKeyboard::KeyCode::KEY_A)
	{
			gracz->Vx=-1;
	}
	else{
		if(klawisz==EventKeyboard::KeyCode::KEY_D)
	{
			gracz->Vx=1;
	}
	}
	}
	}
	if(klawisz==EventKeyboard::KeyCode::KEY_TAB)
	{
		if(gracz->nastepny!=0)
			gracz=gracz->nastepny;
		else gracz=player;
	}
	
}

void HelloWorld::poZwolnieniuKlawisza( cocos2d::EventKeyboard::KeyCode klawisz, cocos2d::Event * event )
{ 
	if(klawisz==EventKeyboard::KeyCode::KEY_S)
	{
			gracz->Vy=0;
	}
	else
	{
		if(klawisz==EventKeyboard::KeyCode::KEY_W)
	{
			gracz->Vy=0;
	}
	else{
		if(klawisz==EventKeyboard::KeyCode::KEY_A)
	{
			gracz->Vx=0;
	}
	else{
		if(klawisz==EventKeyboard::KeyCode::KEY_D)
	{
			gracz->Vx=0;
	}
	}
	}
	}
	
}

Drzewo:: Drzewo ()
{	
		drzewo = cocos2d::Sprite::create("Tree1.png" );	
		drzewo->setPosition(rand()% int (_mapa->getContentSize().height), rand()% int(_mapa->getContentSize().width ));
		auto spriteBody = PhysicsBody::createBox(drzewo->getContentSize(),PhysicsMaterial(0,1,0));
		drzewo->setPhysicsBody(spriteBody);
		warstwa->addChild(drzewo);
};

Gracz::Gracz()
{
		gracz = cocos2d::Sprite::create("Player.png" );		
		gracz->setPosition( 300+ rand()%200, 300+rand()%200);	
		auto spriteBody = PhysicsBody::createBox(gracz->getContentSize(),PhysicsMaterial(0,1,0));/////////////////////////////////fizyka
		gracz->setPhysicsBody(spriteBody);														/////////////////////////////////fizyka
		warstwa->addChild(gracz);
		hp=4;
		
		x=gracz->getPosition().x ;
		y=gracz->getPosition().y ;
		Vx=0;
		Vy=0;
		
};


void Gracz::zmien_hp(int oile)
{
	hp=hp+oile;
	if (hp<=0)
	{

	}
}


Wieza::Wieza()
{
		wieza = cocos2d::Sprite::create("wieza.png" );		
		wieza->setPosition( 200,200);	
		warstwa->addChild(wieza);
		hp=4;
		
		x=wieza->getPosition().x ;
		y=wieza->getPosition().y ;
		
};